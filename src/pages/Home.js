import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
  const data = {
    title: "Zuitt Coding Bootcamp",
    content: "Oppotunities for everyone, everywhere",
    destination: "/courses",
    label: "Enroll",
  };

    return (
      <>
        <Banner data={data} />
        <Highlights />
      </>
    );
}